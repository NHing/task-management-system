﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Note.Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td>标题:</td>
                    <td><asp:TextBox runat="server" ID="txtTitle"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>搜索关键词:</td>
                    <td><asp:TextBox runat="server" ID="txtKeys"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>内容:</td>
                    <td><asp:TextBox runat="server" ID="txtContent"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Button runat="server" ID="Save" Text="添加" OnClick="Save_Click" /></td>
                    <td><asp:Button runat="server" ID="Out" Text="取消并返回" OnClick="Out_Click" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
