﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Note
{
    public partial class Add : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BasicPageInit();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            var keys = this.txtKeys.Text.ToString();
            var title = this.txtTitle.Text.ToString();
            var content = this.txtContent.Text.ToString();

            var sql = string.Format("insert into Note(Keys,Title,Content)values('{0}','{1}','{2}')", keys, title, content);

            

            
                var inserted = DbHelper.AddOrDeleteOrUpdate(sql);
                if (inserted > 0)
                {
                    var res = "<script>alert('添加成功！！')</script>";
                    Response.Write(res);
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    var res = "<script>alert('添加失败！！')</script>";
                    Response.Write(res);
                }
            
            
        }

        protected void Out_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
    }
}