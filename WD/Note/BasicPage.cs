﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Note
{
    public class BasicPage : Page
    {
        public virtual void BasicPageInit()
        {
            var se = Session["UserName"];

            if (se == null || string.IsNullOrEmpty(se.ToString()))
            {
                Response.Redirect("~/Login.aspx");
            }
        }
    }
}