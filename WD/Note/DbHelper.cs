﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Note
{
    public class DbHelper
    {
        private static readonly string conString = "server=.;database=Notes;uid=sa;pwd=123456;";
        
        public static DataTable GetData(string sql)
        {
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conString);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }


        public static int AddOrDeleteOrUpdate(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);
            SqlCommand command = new SqlCommand(sql,connection);
            connection.Open();
            var res = command.ExecuteNonQuery();
            connection.Close();
            return res;
        }
    }
}