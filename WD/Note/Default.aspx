﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Note._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        th{
            text-align:center;
            width:200px;
            height:50px;
        }
        td{
            text-align:center;
            width:200px;
            height:50px;
        }
    </style>
            <asp:Button runat="server" ID="Add" Text="添加" OnClick="Add_Click" />
            <td>&nbsp</td>
            <label>搜索内容：</label>
            <asp:TextBox runat="server" ID="txtSearch"></asp:TextBox>
            <asp:Button runat="server" ID="Search" Text="搜索" OnClick="Search_Click" />
    <asp:GridView runat="server" ID="gv1" OnRowDeleting="gv1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" OnPageIndexChanging="gv1_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="Noteid" HeaderText="编号" />
            <asp:BoundField DataField="Keys" HeaderText="关键词" />
            <asp:BoundField DataField="Title" HeaderText="标题" />
            <asp:BoundField DataField="Content" HeaderText="内容" />
            <asp:ButtonField ButtonType="Link" Text="详情" HeaderText="详情" CommandName="Detail" />
            <asp:CommandField ShowDeleteButton="true" HeaderText="操作" />
        </Columns>
    </asp:GridView>
</asp:Content>
