﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Note
{
    public partial class _Default : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BasicPageInit();
            if (!IsPostBack)
            {
                FillData();
            }
        }

        private void FillData()
        {
            var sql = "select * from Note";
            var dt = DbHelper.GetData(sql);
            gv1.DataSource = dt;
            gv1.DataBind();
        }
        protected void gv1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = GetValue(e.RowIndex, 0);
            
            var sql = string.Format("delete from Note where Noteid = '{0}'", id);
            DbHelper.AddOrDeleteOrUpdate(sql);
            FillData();
           
            
        }

        private string GetValue(int RowIndex,int CellIndex)
        {
            var con = gv1.Rows[RowIndex].Cells[CellIndex];

            var res = con.Controls.Count > 0 ? ((TextBox)con.Controls[0]).Text : con.Text;
            return res;
        }

        protected void gv1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv1.PageIndex = e.NewPageIndex;
            FillData();
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            Response.Redirect("Add.aspx");
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            var search = this.txtSearch.Text.Trim().ToString();

            if (search!=null && !string.IsNullOrWhiteSpace(search))
            {
                var sql = string.Format("select * from Note where Keys like '%{0}%'", search);
                var dt = DbHelper.GetData(sql);
                gv1.DataSource = dt;
                gv1.DataBind();
            }
            else
            {
                FillData();
            }
            
        }

        
    }
}