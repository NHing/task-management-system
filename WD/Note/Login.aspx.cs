﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Note
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            var username = this.Login1.UserName.Trim().ToString();
            var password = this.Login1.Password.Trim().ToString();

            var sql = string.Format("select * from UserInfo where UserName = '{0}' and Pwd = '{1}'", username, password);
            var dt = DbHelper.GetData(sql);
            if (dt.Rows.Count > 0)
            {
                Session["UserName"] = username;
                Response.Redirect("Default.aspx");
            }
        }
    }
}