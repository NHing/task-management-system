create database Notes
go
use Notes
go

create table UserInfo(
	UserId int primary key identity,
	UserName varchar(50) not null,
	Pwd varchar(50) not null
)

create table Note(
	Noteid int primary key identity,
	Keys varchar(100) not null,
	Title varchar(50) not null,
	Content varchar(500) not null
)


insert into UserInfo(UserName,Pwd)values('admin','113'),('111','222'),('sa','123')

insert into Note(Keys,Title,Content)values('关键词1','标题1','内容1'),
('关键词2','标题2','内容2'),
('语文3','标题3','内容3'),
('关键词4','标题4','内容4'),
('前端5','标题5','内容5'),
('关键词6','标题6','内容6'),
('语文7','标题7','内容7'),
('关键词8','标题8','内容8'),
('关键词9','标题9','内容9'),
('语文10','标题10','内容10'),
('关键词11','标题11','内容11'),
('前端12','标题12','内容12'),
('关键词13','标题13','内容13'),
('前端14','标题14','内容14')


