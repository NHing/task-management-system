create database TaskDb
go

use TaskDb

go
create table Users
(
	Id int primary key identity not null,
	Username nvarchar(80) not null,
	Password nvarchar(80) not null,
	IsActived bit not null default 1,
	Isdeleted bit not null default 0,
	Createtime datetime not null default getdate(),
	Updatedtime datetime not null default getdate(),
	Remarks nvarchar(800) null
)

go

create table Task
(
	Id int primary key identity not null,
	TaskName nvarchar(80) not null,
	TaskContent nvarchar(800) not null,
	TaskStatus nvarchar(800) not null default '��ִ��',
	CreatedUserId int not null,
	IsActived bit not null default 1,
	Isdeleted bit not null default 0,
	Createtime datetime not null default getdate(),
	Updatedtime datetime not null default getdate(),
	Remarks nvarchar(800) null
)

go


insert into Users(Username,Password) values ('admin','113'),('user01','113')

go

insert into Task (TaskName,TaskContent,CreatedUserId) values ('1������','��ӥ',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('2������','��ӥ',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('3������','��ӥ',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('4������','��ӥ',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('5������','��ӥ',2)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('6������','��ӥ',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('7������','��ӥ',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('8������','��ӥ',2)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('9������','��ӥ',2)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('10������','��ӥ',2)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('11������','��ӥ',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('12������','��ӥ',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('13������','��ӥ',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('14������','��ӥ',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('15������','��ӥ',1)
insert into Task (TaskName,TaskContent,CreatedUserId) values ('16������','��ӥ',1)
go
