﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace WebApplication1
{
    public class BasicPage:Page
    {
        /// <summary>
        /// 页面初始化时执行函数，用于验证登录状态
        /// </summary>
        public virtual void BasicPageInit()
        {
            var see = Session["UserInfo"];
            if (see == null || string.IsNullOrEmpty(see.ToString()))
            {
                Response.Redirect("~/Login.aspx");
            }
        }
    }
}