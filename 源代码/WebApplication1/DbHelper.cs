﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace WebApplication1
{
    public class DbHelper
    {
        private static readonly string conString = "server=.;database=TaskDb;uid=sa;pwd=123456;";
        public static DataTable GetData(string sql)
        {
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conString);

            var dt = new DataTable();

            adapter.Fill(dt);

            return dt;
        }

        public static int ExeOperation(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);

            SqlCommand command = new SqlCommand(sql, connection);

            connection.Open();

            var count=command.ExecuteNonQuery();

            connection.Close();

            return count;

        }
    }
}