﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        th{
            text-align:center;
        }
        td{
            width:200px;
            text-align:center;
        }
    </style>
    <asp:Button runat="server" ID="btnAdd" Text="添加" OnClick="btnAdd_Click"/>
    <asp:GridView runat="server" ID="GrdTask" AllowPaging="true" AutoGenerateColumns="false"OnRowEditing="GrdTask_RowEditing" OnRowDeleting="GrdTask_RowDeleting" OnPageIndexChanging="GrdTask_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" />
            <asp:BoundField DataField="TaskName" HeaderText="任务名称" />
            <asp:BoundField DataField="TaskContent" HeaderText="任务内容" />
            <asp:BoundField DataField="TaskStatus" HeaderText="状态" />
            <%--<asp:BoundField DataField="CreatedUserId" HeaderText="创建者" />--%>
            <asp:BoundField DataField="Remarks" HeaderText="备注" />
            <asp:ButtonField runat="server" CommandName="btnDetail" HeaderText="详情" ButtonType="Link" Text="详情" />
            <asp:CommandField HeaderText="操作" ShowEditButton="true" ShowDeleteButton="true"/>
        </Columns>
    </asp:GridView>

</asp:Content>
