﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : BasicPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BasicPageInit();
            if (!IsPostBack)
            {
                FillData();

            }
        }

        private void FillData()
        {
            var sql = "select * from task";
            var dt = DbHelper.GetData(sql);
            GrdTask.DataSource = dt;
            GrdTask.DataBind();
        }

        private string GetVal(int rowIndex,int colIndex)
        {
            var control = GrdTask.Rows[rowIndex].Cells[colIndex];

            var res = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return res;
        }

        protected void GrdTask_RowEditing(object sender, GridViewEditEventArgs e)
        {
            Response.Redirect("TaskEditNew.aspx?id="+GetVal(e.NewEditIndex,0));
        }

        protected void GrdTask_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var id = GetVal(e.RowIndex, 0);

            var sql = string.Format("delete from Task where Id={0}", id);

            var deleted = DbHelper.ExeOperation(sql);

            FillData();
        }

        protected void GrdTask_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrdTask.PageIndex = e.NewPageIndex;
            FillData();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("TaskEditNew.aspx");
        }
    }
}