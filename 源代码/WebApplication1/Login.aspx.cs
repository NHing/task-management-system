﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var se = Session["UserInfo"];
            if(se!=null && !string.IsNullOrEmpty(se.ToString()))
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            var username = Login1.UserName;
            var password = Login1.Password;

            var sql = string.Format("select * from Users where Username='{0}' and Password='{1}'", username, password);

            var users = DbHelper.GetData(sql);

            if (users.Rows.Count > 0)
            {
                Session["UserInfo"] = username;
                Session["UserId"] = users.Rows[0].ItemArray[0].ToString();
                Response.Redirect("Default.aspx");
            }

        }
    }
}