﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TaskDetails.aspx.cs" Inherits="WebApplication1.TaskDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <table>
            <tr>
                <td><label>任务Id</label></td>
                <td><label runat="server" id="lblId"></label></td>
            </tr>
            <tr>
                <td><label>任务名称</label></td>
                <td><label runat="server" id="lblName"></label></td>
            </tr>
            <tr>
                <td><label>任务内容</label></td>
                <td><label runat="server" id="lblContent"></label></td>
            </tr>
            <tr>
                <td><label>任务状态</label></td>
                <td><label runat="server" id="lblStatus"></label></td>
            </tr>
            <tr>
                <td><label>任务创建者</label></td>
                <td><label runat="server" id="lblUser"></label></td>
            </tr>
            <tr>
                <td><label>任务创建时间</label></td>
                <td><label runat="server" id="lblCreatedTime"></label></td>
            </tr>
            <tr>
                <td><label>备注</label></td>
                <td><label runat="server" id="lblRemarks"></label></td>
            </tr>
        </table>
    </div>
</asp:Content>
