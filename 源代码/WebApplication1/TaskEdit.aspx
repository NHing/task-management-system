﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskEdit.aspx.cs" Inherits="WebApplication1.TaskEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr hidden="hidden">
                    <td>
                        <label>Id</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtId" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>任务名称：</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>任务内容：</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtContent" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>备注：</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="保存"/>
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="取消并返回" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
