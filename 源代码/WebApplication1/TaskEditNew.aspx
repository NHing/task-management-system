﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TaskEditNew.aspx.cs" Inherits="WebApplication1.TaskEditNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <div>
            <table>
                <tr hidden="hidden">
                    <td>
                        <label>Id</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtId" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>任务名称：</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>任务内容：</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtContent" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>备注：</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemarks" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="保存"/>
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="取消并返回" />
                    </td>
                </tr>
            </table>
        </div>
</asp:Content>
